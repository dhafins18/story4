from django import forms

class JadwalForm(forms.Form):
    nama = forms.CharField(label = 'Nama Kegiatan',
        required = True, max_length = 255,
        widget = forms.TextInput(attrs = {'placeholder' : 'contoh: lab PPW',
        'class' : 'col-12 form-control'}))
    kategori = forms.CharField(label = 'Kategori Kegiatan',
        required = True, max_length = 255,
        widget = forms.TextInput(attrs = {'placeholder' : 'contoh: akademis',
        'class' : 'col-12 form-control'}))
    tempat = forms.CharField(label = 'Tempat Kegiatan',
        required = True, max_length = 255,
        widget = forms.TextInput(attrs = {'placeholder' : 'contoh: 2.2601',
        'class' : 'col-12 form-control'}))
    tanggal = forms.DateTimeField(label = 'Tanggal Kegiatan',
        required = True, input_formats = ['%Y-%m-%dT%H:%M'],
        widget = forms.DateTimeInput(attrs = {'type' : 'datetime-local',
        'class' : 'col-12 form-control'}))
